package de.jottyfan.timetrack.spy;

import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JOptionPane;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.sun.net.httpserver.HttpServer;

import de.jottyfan.timetrack.spy.rest.Daylist;

/**
 * 
 * @author henkej
 *
 */
public class Standalone {

  public void run(String url) throws URISyntaxException {
    ResourceConfig rc = new ResourceConfig();
    rc.registerClasses(Daylist.class);
    HttpServer server = JdkHttpServerFactory.createHttpServer(new URI(url), rc, true);
    JOptionPane.showMessageDialog(null, "Server anhalten");
    server.stop(0);
  }

  public static void main(String args[]) {
    try {
      System.out.println("call it by curl to receive json: curl localhost:8901/daylist -H \"Accept: application/json\"");
      new Standalone().run("http://localhost:8901/");
      System.exit(0);
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }
}
