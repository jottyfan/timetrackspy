package de.jottyfan.timetrack.spy.json;

import com.google.gson.Gson;

/**
 * 
 * @author henkej
 *
 */
public class GsonConverter {
  /**
   * generate a standardized json structure for errors
   * 
   * @param e the exception
   * @return a json representation of the exception
   */
  public String generateError(Exception e) {
    String jsonString = new Gson().toJson(e.getMessage());
    return jsonString;
  }
}
