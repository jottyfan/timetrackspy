package de.jottyfan.timetrack.spy.db;

import static de.jottyfan.timetrack.db.done.Tables.T_DONE;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SelectConditionStep;
import org.jooq.exception.DataAccessException;

import de.jottyfan.timetrack.db.done.tables.records.TDoneRecord;

/**
 * 
 * @author henkej
 *
 */
public class DoneGateway extends JooqGateway {

  /**
   * get all entries older than date
   * 
   * @param date the date to compare with
   * @return a html table of found records
   * @throws SQLException
   * @throws ClassNotFoundException
   * @throws DataAccessException
   */
  public String getHtmlTableOfDate(LocalDateTime dateTime) throws DataAccessException, ClassNotFoundException, SQLException {
    return getTableOfDate(dateTime).formatHTML();
  }
  
  /**
   * get all entries older than date
   * 
   * @param date the date to compare with
   * @return a json table of found records
   * @throws SQLException
   * @throws ClassNotFoundException
   * @throws DataAccessException
   */
  public String getJsonTableOfDate(LocalDateTime dateTime) throws DataAccessException, ClassNotFoundException, SQLException {
    return getTableOfDate(dateTime).formatJSON();
  }
  
  /**
   * get all entries older than date
   * 
   * @param date the date to compare with
   * @return a csv table of found records
   * @throws SQLException
   * @throws ClassNotFoundException
   * @throws DataAccessException
   */
  public String getCsvTableOfDate(LocalDateTime dateTime) throws DataAccessException, ClassNotFoundException, SQLException {
    return getTableOfDate(dateTime).formatCSV();
  }

  /**
   * get all entries older than date
   * 
   * @param date the date to compare with
   * @return result set of found records
   * @throws SQLException
   * @throws ClassNotFoundException
   * @throws DataAccessException
   */
  private Result<TDoneRecord> getTableOfDate(LocalDateTime dateTime) throws DataAccessException, ClassNotFoundException, SQLException{
    try (DSLContext jooq = getJooq()) {
      SelectConditionStep<TDoneRecord> sql = jooq.selectFrom(T_DONE)
          .where(T_DONE.TIME_FROM.ge(Timestamp.valueOf(dateTime)));
      System.out.println(sql.toString());
      return sql.fetch();
    }
  }
}
