package de.jottyfan.timetrack.spy.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

/**
 * 
 * @author henkej
 *
 */
public abstract class JooqGateway {

  private final String url;
  
  public JooqGateway() {
    this.url = "jdbc:postgresql://localhost:5432/timetrack?user=anyone&password=";
  }
  
  /**
   * get the jooq dsl context
   * 
   * @return the jooq dsl context
   * @throws ClassNotFoundException 
   * @throws SQLException 
   */
  public DSLContext getJooq() throws ClassNotFoundException, SQLException {
    Class.forName("org.postgresql.Driver");
    Connection con = DriverManager.getConnection(url);
    DSLContext jooq = DSL.using(con, SQLDialect.POSTGRES);
    return jooq;
  }
}
