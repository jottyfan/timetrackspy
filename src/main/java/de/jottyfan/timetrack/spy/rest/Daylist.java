package de.jottyfan.timetrack.spy.rest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import de.jottyfan.timetrack.spy.csv.CsvConverter;
import de.jottyfan.timetrack.spy.db.DoneGateway;
import de.jottyfan.timetrack.spy.html.HtmlConverter;
import de.jottyfan.timetrack.spy.json.GsonConverter;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * 
 * @author henkej
 *
 */
@Path("/daylist")
public class Daylist {

  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String getUsage() {
    return "usage: /daylist/query?date=yyyyMMdd, where yyyy is a year (4 digits), MM a month (2 digits) and dd a day (2 digits), e.g. 20121224";
  }

  @GET
  @Path("/query")
  @Produces(MediaType.TEXT_HTML)
  public Response getHtml(@DefaultValue("now") @QueryParam("date") final String date) {
    String result = date; // dummy
    try {
      LocalDateTime ldt = LocalDateTime.now();
      if (!"now".contentEquals(date)) {
        ldt = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyyMMdd")).atStartOfDay();  
      }
      result = new DoneGateway().getHtmlTableOfDate(ldt);
    } catch (Exception e) {
      result = new HtmlConverter().generateError(e);
    }
    return Response.status(200).entity(new HtmlConverter().embed(result, date)).build();
  }
  
  @GET
  @Path("/query")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getJson(@DefaultValue("now") @QueryParam("date") final String date) {
    String result = date; // dummy
    try {
      LocalDateTime ldt = LocalDateTime.now();
      if (!"now".contentEquals(date)) {
        ldt = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyyMMdd")).atStartOfDay();  
      }
      result = new DoneGateway().getJsonTableOfDate(ldt);
    } catch (Exception e) {
      result = new GsonConverter().generateError(e);
    }
    return Response.status(200).entity(result).build();
  }
  
  @GET
  @Path("/query")
  @Produces(MediaType.TEXT_PLAIN)
  public Response getCsv(@DefaultValue("now") @QueryParam("date") final String date) {
    String result = date; // dummy
    try {
      LocalDateTime ldt = LocalDateTime.now();
      if (!"now".contentEquals(date)) {
        ldt = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyyMMdd")).atStartOfDay();  
      }
      result = new DoneGateway().getCsvTableOfDate(ldt);
    } catch (Exception e) {
      result = new CsvConverter().generateError(e);
    }
    return Response.status(200).entity(result).build();
  }
}
