package de.jottyfan.timetrack.spy.csv;

/**
 * 
 * @author henkej
 *
 */
public class CsvConverter {

  /**
   * simple error as first cell of a csv without any more columns
   * 
   * @param e the error
   * @return the csv file content
   */
  public String generateError(Exception e) {
    return e.getMessage();
  }
}
