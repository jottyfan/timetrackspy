package de.jottyfan.timetrack.spy.html;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 
 * @author henkej
 *
 */
public class HtmlConverter {
  /**
   * embeds inner into a basic html structure
   * 
   * @param inner the inner
   * @return the final html file
   */
  public String embed(String inner, String title) {
    StringBuilder buf = new StringBuilder();
    buf.append("<html><head><title>").append(title).append("</title></head>");
    buf.append("<body>").append(inner).append("</body></html>");
    return buf.toString();
  }

  /**
   * generate a html error representation
   * @param e the exception
   * @return the final html file
   */
  public String generateError(Exception e) {
    StringWriter stringWriter = new StringWriter();
    stringWriter.append("<div style=\"color: red\">");
    PrintWriter printWriter = new PrintWriter(stringWriter);
    e.printStackTrace(printWriter);
    stringWriter.append("</div>");
    return embed(stringWriter.toString(), "Error");
  }
}
