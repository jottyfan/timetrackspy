/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.timetrack.db.note.enums;


import de.jottyfan.timetrack.db.note.Note;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.EnumType;
import org.jooq.Schema;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public enum EnumNotetype implements EnumType {

    Administration("Administration"),

    HowTo("HowTo");

    private final String literal;

    private EnumNotetype(String literal) {
        this.literal = literal;
    }

    @Override
    public Catalog getCatalog() {
        return getSchema() == null ? null : getSchema().getCatalog();
    }

    @Override
    public Schema getSchema() {
        return Note.NOTE;
    }

    @Override
    public String getName() {
        return "enum_notetype";
    }

    @Override
    public String getLiteral() {
        return literal;
    }
}
