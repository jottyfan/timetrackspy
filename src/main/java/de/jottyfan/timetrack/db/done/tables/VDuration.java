/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.timetrack.db.done.tables;


import de.jottyfan.timetrack.db.done.Done;
import de.jottyfan.timetrack.db.done.tables.records.VDurationRecord;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row7;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.YearToSecond;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDuration extends TableImpl<VDurationRecord> {

    private static final long serialVersionUID = -578535096;

    /**
     * The reference instance of <code>done.v_duration</code>
     */
    public static final VDuration V_DURATION = new VDuration();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VDurationRecord> getRecordType() {
        return VDurationRecord.class;
    }

    /**
     * The column <code>done.v_duration.day</code>.
     */
    public final TableField<VDurationRecord, String> DAY = createField(DSL.name("day"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>done.v_duration.duration</code>.
     */
    public final TableField<VDurationRecord, YearToSecond> DURATION = createField(DSL.name("duration"), org.jooq.impl.SQLDataType.INTERVAL, this, "");

    /**
     * The column <code>done.v_duration.project_name</code>.
     */
    public final TableField<VDurationRecord, String> PROJECT_NAME = createField(DSL.name("project_name"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>done.v_duration.module_name</code>.
     */
    public final TableField<VDurationRecord, String> MODULE_NAME = createField(DSL.name("module_name"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>done.v_duration.job_name</code>.
     */
    public final TableField<VDurationRecord, String> JOB_NAME = createField(DSL.name("job_name"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>done.v_duration.login</code>.
     */
    public final TableField<VDurationRecord, String> LOGIN = createField(DSL.name("login"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>done.v_duration.fk_login</code>.
     */
    public final TableField<VDurationRecord, Integer> FK_LOGIN = createField(DSL.name("fk_login"), org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * Create a <code>done.v_duration</code> table reference
     */
    public VDuration() {
        this(DSL.name("v_duration"), null);
    }

    /**
     * Create an aliased <code>done.v_duration</code> table reference
     */
    public VDuration(String alias) {
        this(DSL.name(alias), V_DURATION);
    }

    /**
     * Create an aliased <code>done.v_duration</code> table reference
     */
    public VDuration(Name alias) {
        this(alias, V_DURATION);
    }

    private VDuration(Name alias, Table<VDurationRecord> aliased) {
        this(alias, aliased, null);
    }

    private VDuration(Name alias, Table<VDurationRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> VDuration(Table<O> child, ForeignKey<O, VDurationRecord> key) {
        super(child, key, V_DURATION);
    }

    @Override
    public Schema getSchema() {
        return Done.DONE;
    }

    @Override
    public VDuration as(String alias) {
        return new VDuration(DSL.name(alias), this);
    }

    @Override
    public VDuration as(Name alias) {
        return new VDuration(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public VDuration rename(String name) {
        return new VDuration(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VDuration rename(Name name) {
        return new VDuration(name, null);
    }

    // -------------------------------------------------------------------------
    // Row7 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row7<String, YearToSecond, String, String, String, String, Integer> fieldsRow() {
        return (Row7) super.fieldsRow();
    }
}
