/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.timetrack.db.done.tables.records;


import de.jottyfan.timetrack.db.done.tables.VDaylimits;

import java.sql.Timestamp;
import java.time.OffsetDateTime;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDaylimitsRecord extends TableRecordImpl<VDaylimitsRecord> implements Record4<Timestamp, OffsetDateTime, String, Integer> {

    private static final long serialVersionUID = 1544678482;

    /**
     * Setter for <code>done.v_daylimits.work_start</code>.
     */
    public void setWorkStart(Timestamp value) {
        set(0, value);
    }

    /**
     * Getter for <code>done.v_daylimits.work_start</code>.
     */
    public Timestamp getWorkStart() {
        return (Timestamp) get(0);
    }

    /**
     * Setter for <code>done.v_daylimits.work_end</code>.
     */
    public void setWorkEnd(OffsetDateTime value) {
        set(1, value);
    }

    /**
     * Getter for <code>done.v_daylimits.work_end</code>.
     */
    public OffsetDateTime getWorkEnd() {
        return (OffsetDateTime) get(1);
    }

    /**
     * Setter for <code>done.v_daylimits.day</code>.
     */
    public void setDay(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>done.v_daylimits.day</code>.
     */
    public String getDay() {
        return (String) get(2);
    }

    /**
     * Setter for <code>done.v_daylimits.fk_login</code>.
     */
    public void setFkLogin(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>done.v_daylimits.fk_login</code>.
     */
    public Integer getFkLogin() {
        return (Integer) get(3);
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<Timestamp, OffsetDateTime, String, Integer> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<Timestamp, OffsetDateTime, String, Integer> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<Timestamp> field1() {
        return VDaylimits.V_DAYLIMITS.WORK_START;
    }

    @Override
    public Field<OffsetDateTime> field2() {
        return VDaylimits.V_DAYLIMITS.WORK_END;
    }

    @Override
    public Field<String> field3() {
        return VDaylimits.V_DAYLIMITS.DAY;
    }

    @Override
    public Field<Integer> field4() {
        return VDaylimits.V_DAYLIMITS.FK_LOGIN;
    }

    @Override
    public Timestamp component1() {
        return getWorkStart();
    }

    @Override
    public OffsetDateTime component2() {
        return getWorkEnd();
    }

    @Override
    public String component3() {
        return getDay();
    }

    @Override
    public Integer component4() {
        return getFkLogin();
    }

    @Override
    public Timestamp value1() {
        return getWorkStart();
    }

    @Override
    public OffsetDateTime value2() {
        return getWorkEnd();
    }

    @Override
    public String value3() {
        return getDay();
    }

    @Override
    public Integer value4() {
        return getFkLogin();
    }

    @Override
    public VDaylimitsRecord value1(Timestamp value) {
        setWorkStart(value);
        return this;
    }

    @Override
    public VDaylimitsRecord value2(OffsetDateTime value) {
        setWorkEnd(value);
        return this;
    }

    @Override
    public VDaylimitsRecord value3(String value) {
        setDay(value);
        return this;
    }

    @Override
    public VDaylimitsRecord value4(Integer value) {
        setFkLogin(value);
        return this;
    }

    @Override
    public VDaylimitsRecord values(Timestamp value1, OffsetDateTime value2, String value3, Integer value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VDaylimitsRecord
     */
    public VDaylimitsRecord() {
        super(VDaylimits.V_DAYLIMITS);
    }

    /**
     * Create a detached, initialised VDaylimitsRecord
     */
    public VDaylimitsRecord(Timestamp workStart, OffsetDateTime workEnd, String day, Integer fkLogin) {
        super(VDaylimits.V_DAYLIMITS);

        set(0, workStart);
        set(1, workEnd);
        set(2, day);
        set(3, fkLogin);
    }
}
