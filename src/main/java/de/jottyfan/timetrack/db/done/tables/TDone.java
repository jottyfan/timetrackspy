/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.timetrack.db.done.tables;


import de.jottyfan.timetrack.db.done.Done;
import de.jottyfan.timetrack.db.done.Indexes;
import de.jottyfan.timetrack.db.done.tables.records.TDoneRecord;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row8;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TDone extends TableImpl<TDoneRecord> {

    private static final long serialVersionUID = -2079648247;

    /**
     * The reference instance of <code>done.t_done</code>
     */
    public static final TDone T_DONE = new TDone();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TDoneRecord> getRecordType() {
        return TDoneRecord.class;
    }

    /**
     * The column <code>done.t_done.lastchange</code>.
     */
    public final TableField<TDoneRecord, Timestamp> LASTCHANGE = createField(DSL.name("lastchange"), org.jooq.impl.SQLDataType.TIMESTAMP.defaultValue(org.jooq.impl.DSL.field("now()", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>done.t_done.pk</code>.
     */
    public final TableField<TDoneRecord, Integer> PK = createField(DSL.name("pk"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>done.t_done.time_from</code>.
     */
    public final TableField<TDoneRecord, Timestamp> TIME_FROM = createField(DSL.name("time_from"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>done.t_done.time_until</code>.
     */
    public final TableField<TDoneRecord, Timestamp> TIME_UNTIL = createField(DSL.name("time_until"), org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>done.t_done.fk_project</code>.
     */
    public final TableField<TDoneRecord, Integer> FK_PROJECT = createField(DSL.name("fk_project"), org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>done.t_done.fk_module</code>.
     */
    public final TableField<TDoneRecord, Integer> FK_MODULE = createField(DSL.name("fk_module"), org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>done.t_done.fk_job</code>.
     */
    public final TableField<TDoneRecord, Integer> FK_JOB = createField(DSL.name("fk_job"), org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>done.t_done.fk_login</code>.
     */
    public final TableField<TDoneRecord, Integer> FK_LOGIN = createField(DSL.name("fk_login"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>done.t_done</code> table reference
     */
    public TDone() {
        this(DSL.name("t_done"), null);
    }

    /**
     * Create an aliased <code>done.t_done</code> table reference
     */
    public TDone(String alias) {
        this(DSL.name(alias), T_DONE);
    }

    /**
     * Create an aliased <code>done.t_done</code> table reference
     */
    public TDone(Name alias) {
        this(alias, T_DONE);
    }

    private TDone(Name alias, Table<TDoneRecord> aliased) {
        this(alias, aliased, null);
    }

    private TDone(Name alias, Table<TDoneRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> TDone(Table<O> child, ForeignKey<O, TDoneRecord> key) {
        super(child, key, T_DONE);
    }

    @Override
    public Schema getSchema() {
        return Done.DONE;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.T_DONE_PKEY);
    }

    @Override
    public TDone as(String alias) {
        return new TDone(DSL.name(alias), this);
    }

    @Override
    public TDone as(Name alias) {
        return new TDone(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public TDone rename(String name) {
        return new TDone(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TDone rename(Name name) {
        return new TDone(name, null);
    }

    // -------------------------------------------------------------------------
    // Row8 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row8<Timestamp, Integer, Timestamp, Timestamp, Integer, Integer, Integer, Integer> fieldsRow() {
        return (Row8) super.fieldsRow();
    }
}
