/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.timetrack.db.done.tables.records;


import de.jottyfan.timetrack.db.done.tables.VDuration;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record7;
import org.jooq.Row7;
import org.jooq.impl.TableRecordImpl;
import org.jooq.types.YearToSecond;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDurationRecord extends TableRecordImpl<VDurationRecord> implements Record7<String, YearToSecond, String, String, String, String, Integer> {

    private static final long serialVersionUID = 794239622;

    /**
     * Setter for <code>done.v_duration.day</code>.
     */
    public void setDay(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>done.v_duration.day</code>.
     */
    public String getDay() {
        return (String) get(0);
    }

    /**
     * Setter for <code>done.v_duration.duration</code>.
     */
    public void setDuration(YearToSecond value) {
        set(1, value);
    }

    /**
     * Getter for <code>done.v_duration.duration</code>.
     */
    public YearToSecond getDuration() {
        return (YearToSecond) get(1);
    }

    /**
     * Setter for <code>done.v_duration.project_name</code>.
     */
    public void setProjectName(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>done.v_duration.project_name</code>.
     */
    public String getProjectName() {
        return (String) get(2);
    }

    /**
     * Setter for <code>done.v_duration.module_name</code>.
     */
    public void setModuleName(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>done.v_duration.module_name</code>.
     */
    public String getModuleName() {
        return (String) get(3);
    }

    /**
     * Setter for <code>done.v_duration.job_name</code>.
     */
    public void setJobName(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>done.v_duration.job_name</code>.
     */
    public String getJobName() {
        return (String) get(4);
    }

    /**
     * Setter for <code>done.v_duration.login</code>.
     */
    public void setLogin(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>done.v_duration.login</code>.
     */
    public String getLogin() {
        return (String) get(5);
    }

    /**
     * Setter for <code>done.v_duration.fk_login</code>.
     */
    public void setFkLogin(Integer value) {
        set(6, value);
    }

    /**
     * Getter for <code>done.v_duration.fk_login</code>.
     */
    public Integer getFkLogin() {
        return (Integer) get(6);
    }

    // -------------------------------------------------------------------------
    // Record7 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row7<String, YearToSecond, String, String, String, String, Integer> fieldsRow() {
        return (Row7) super.fieldsRow();
    }

    @Override
    public Row7<String, YearToSecond, String, String, String, String, Integer> valuesRow() {
        return (Row7) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return VDuration.V_DURATION.DAY;
    }

    @Override
    public Field<YearToSecond> field2() {
        return VDuration.V_DURATION.DURATION;
    }

    @Override
    public Field<String> field3() {
        return VDuration.V_DURATION.PROJECT_NAME;
    }

    @Override
    public Field<String> field4() {
        return VDuration.V_DURATION.MODULE_NAME;
    }

    @Override
    public Field<String> field5() {
        return VDuration.V_DURATION.JOB_NAME;
    }

    @Override
    public Field<String> field6() {
        return VDuration.V_DURATION.LOGIN;
    }

    @Override
    public Field<Integer> field7() {
        return VDuration.V_DURATION.FK_LOGIN;
    }

    @Override
    public String component1() {
        return getDay();
    }

    @Override
    public YearToSecond component2() {
        return getDuration();
    }

    @Override
    public String component3() {
        return getProjectName();
    }

    @Override
    public String component4() {
        return getModuleName();
    }

    @Override
    public String component5() {
        return getJobName();
    }

    @Override
    public String component6() {
        return getLogin();
    }

    @Override
    public Integer component7() {
        return getFkLogin();
    }

    @Override
    public String value1() {
        return getDay();
    }

    @Override
    public YearToSecond value2() {
        return getDuration();
    }

    @Override
    public String value3() {
        return getProjectName();
    }

    @Override
    public String value4() {
        return getModuleName();
    }

    @Override
    public String value5() {
        return getJobName();
    }

    @Override
    public String value6() {
        return getLogin();
    }

    @Override
    public Integer value7() {
        return getFkLogin();
    }

    @Override
    public VDurationRecord value1(String value) {
        setDay(value);
        return this;
    }

    @Override
    public VDurationRecord value2(YearToSecond value) {
        setDuration(value);
        return this;
    }

    @Override
    public VDurationRecord value3(String value) {
        setProjectName(value);
        return this;
    }

    @Override
    public VDurationRecord value4(String value) {
        setModuleName(value);
        return this;
    }

    @Override
    public VDurationRecord value5(String value) {
        setJobName(value);
        return this;
    }

    @Override
    public VDurationRecord value6(String value) {
        setLogin(value);
        return this;
    }

    @Override
    public VDurationRecord value7(Integer value) {
        setFkLogin(value);
        return this;
    }

    @Override
    public VDurationRecord values(String value1, YearToSecond value2, String value3, String value4, String value5, String value6, Integer value7) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VDurationRecord
     */
    public VDurationRecord() {
        super(VDuration.V_DURATION);
    }

    /**
     * Create a detached, initialised VDurationRecord
     */
    public VDurationRecord(String day, YearToSecond duration, String projectName, String moduleName, String jobName, String login, Integer fkLogin) {
        super(VDuration.V_DURATION);

        set(0, day);
        set(1, duration);
        set(2, projectName);
        set(3, moduleName);
        set(4, jobName);
        set(5, login);
        set(6, fkLogin);
    }
}
